from collections import UserDict
from datetime import datetime
import re


def create_date(*, year, month, day):
    return datetime(year=year, month=month, day=day).date()


class Field:
    pass


class Name(Field):
    pass


class Field:
    def __init__(self, _value: str):
        self._value = _value
        self._value = _value

    def __repr__(self):
        return self._value

    @property
    def value(self):
        return self._value

    @Field.value.setter
    def value(self, value):
        self._value = value


class Phone(Field):
    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        def value(self, value):
            new_value = re.findall(r"(?:\+\d{2})?\d{3,4}\D?\d{3}\D?\d{3}, value")
            for i in new_value: new_value = i
            if len(value) >= 9 and len(new_value) > 0:
                self._value = new_value
            else:
                self._value = None

        self._value = value


class Birthday(Field):

    @property
    def value(self, value):
        return datetime.strptime(value, "%d-%m-%Y")

    @value.setter
    def value(self, value):
        self.value = datetime.strptime(value, "%d-%m-%Y")

    def __repr__(self):
        return self._value


class Record:

    def init(self, name, phones, birthday):
        self.name = name
        self.phones = phones
        self.birthday = birthday

    def days_to_birthday(self):
        if self.birthday is not None:
            birthday: datetime.datetime = self.birthday.value.date
            current_birthday = datetime(year=datetime.now.year, month=datetime.month.birthday,
                                        day=datetime.day.birthday)
            if birthday < current_birthday:
                current_birthday = create_date(year=current_birthday.year + 1, month=current_birthday.month,
                                               day=current_birthday.day)
                return (current_birthday - birthday).days

    def delete(self, phone):
        self.phones.remove(phone)

    def add(self, phone):
        self.phones.append(phone)

    def update(self, old_phone, new_phone):
        self.phones.remove(old_phone)
        self.phones.append(new_phone)
        pass


class AddressBook(UserDict):
    __items_per_page = 20

    def items_per_page(self, value):
        self.__items_per_page = value

    items_per_page = property(fget=none, fset=items_per_page)

    def add_record(self, record):
        self.data[record.name] = record.phones

    def __iter__(self):
        return self

    def __next__(self):
        records = list(self.data.items())
        start_index = self.page * self.__items_per_page
        end_index = (self.page + 1) * self.__items_per_page
        self.page +=1
        to_return = None
        if len(records) > end_index:
           to_return =  records[start_index:end_index]
        else:
            if len(records) > start_index:
                to_return = records[start_index:len(records)]
            else:
                to_return = records[:-1]
            return [{record[1],record[0]} for record in to_return]



phone = Phone("0993632605")
second_phone = Phone("672542885")
record = Record("Saska", [phone])
record.add(second_phone)
record.delete(phone)
record.update(second_phone, Phone('0954015320cd'))
first_address_book = AddressBook()
first_address_book.add_record(record)

print(first_address_book['Saska'][0].phone)

# a.add()
